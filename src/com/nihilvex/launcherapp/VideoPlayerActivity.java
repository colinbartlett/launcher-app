package com.nihilvex.launcherapp;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class VideoPlayerActivity extends Activity implements OnClickListener, OnCompletionListener {

	private VideoView mVideoView;
	private int position = 0;
	private ProgressDialog progressDialog;
	private MediaController mediaControls;
	
	private static String LOCKED_STATE= "Locked State";
	private static String PLAYBACK_COMPLETED = "Playback Finished";
	private boolean mIsSubscriptionPayed;
	
	private RelativeLayout mOverlayLayout;
	private ArrayList<Integer> mButtonsClicked = new ArrayList<Integer>();
	private SharedPreferences mPrefs;
	
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		

		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		if(mPrefs.getBoolean(PLAYBACK_COMPLETED, false)) {
			setContentView(R.layout.activity_watched);
			mOverlayLayout = (RelativeLayout) findViewById(R.id.locked_layout);
		} else {
		
			 requestWindowFeature(Window.FEATURE_NO_TITLE);
		        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		            WindowManager.LayoutParams.FLAG_FULLSCREEN);
			// Get the layout from video_main.xml
			setContentView(R.layout.activity_unwatched);
			
			mOverlayLayout = (RelativeLayout) findViewById(R.id.locked_layout);
			mIsSubscriptionPayed = mPrefs.getBoolean(LOCKED_STATE, true);
			
			if (mediaControls == null) {
				mediaControls = new MediaController(VideoPlayerActivity.this);
			}

			// Find your VideoView in your video_main.xml layout
			mVideoView = (VideoView) findViewById(R.id.video_view);
	
			// Create a progressbar while loading the video
			progressDialog = new ProgressDialog(VideoPlayerActivity.this);
			// Set progressbar title
			progressDialog.setTitle(getString(R.string.loading_title));
			// Set progressbar message
			progressDialog.setMessage(getString(R.string.loading));
			progressDialog.setCancelable(false);
			// Show progressbar
			progressDialog.show();
	
			try {
				mVideoView.setMediaController(mediaControls);
				//myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.kitkat));
				mVideoView.setVideoPath(Environment.getExternalStorageDirectory() + "/video_demo.mp4");
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
	
			mVideoView.setOnCompletionListener(this);
			mVideoView.requestFocus();
			mVideoView.setOnPreparedListener(new OnPreparedListener() {
				// Close the progress bar and play the video
				public void onPrepared(MediaPlayer mp) {
					progressDialog.dismiss();
					mVideoView.seekTo(position);
					if (position == 0) {
						if(isSubscriptionActive()) {
							mVideoView.start();
						}
					} else {
						mVideoView.pause();
					}
				}
			});
			
			checkSubscriptionStatus();
		}
	}
	

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.i("VideoPlayerActivity","onResume");
		checkSubscriptionStatus();
	}

	// Checks if "user has payed subscription"
	private void checkSubscriptionStatus() {
		
		if(!isSubscriptionActive()) {
			// No active subscription
			mOverlayLayout.setVisibility(View.VISIBLE);
			if(mVideoView != null) {
				mVideoView.pause();
				mVideoView.setMediaController(null);
			}
		} else {
			// Subscription is payed
			mOverlayLayout.setVisibility(View.GONE);
			if(mVideoView != null) {
				mVideoView.setMediaController(mediaControls);
				if(!mVideoView.isPlaying()) {
					// Start video again when subscription is payed
					mVideoView.start();
				}
			}
		}
	}

	// Implement subscription check in this method
	private boolean isSubscriptionActive() {

		mIsSubscriptionPayed = mPrefs.getBoolean(LOCKED_STATE, true);
		
		return mIsSubscriptionPayed;
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		if(mVideoView != null) {
			savedInstanceState.putInt("Position", mVideoView.getCurrentPosition());
			mVideoView.pause();
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(mVideoView != null) {
			position = savedInstanceState.getInt("Position");
			mVideoView.seekTo(position);
		}
		
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		//myVideoView.start();
		Log.w("VideoPlayerActivity","onConfigurationChanged");
		//checkIfLocked();
	}


	@Override
	public void onClick(View v) {
		// onClick listener of the buttons in @layot/overlay_layout.xml
		
		if(!mButtonsClicked.contains(v.getId())) {
			// If not already pressed add button id to the list of pressed buttons
			mButtonsClicked.add(v.getId());
		}

		if(mButtonsClicked.size() == 4) {
			// Clear list so it can be activated again
			mButtonsClicked.clear();
			Log.w("VideoPlayerActivity", "App should lock or unlock");
			// Change subscription status
			changeSubscriptionStatus(!isSubscriptionActive());
		}
		
	}
	
	
	public void changeSubscriptionStatus(boolean status) {
		
		mIsSubscriptionPayed = status;
		mPrefs.edit().putBoolean(LOCKED_STATE, mIsSubscriptionPayed).commit();
		
		checkSubscriptionStatus();
		
	}


	@Override
	public void onCompletion(MediaPlayer mp) {
		// On playback completion
		Log.w("OnCompletionListener","Video finished");
		mPrefs.edit().putBoolean(PLAYBACK_COMPLETED, true).commit();
	}
}
